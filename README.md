# key-exchange

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`key-exchange` provides the key exchange operations for CoBox.

## Install 
```
npm i -g @coboxcoop/key-exchange
```

## Usage
`const invite = require('cobox-key-exchange')`

### `const inviteCode = create(opts)`

`opts = { publicKey, address, [encryptionKey], [name] }`

Creates an invite code.  Returns a buffer.
- `publicKey` - the public encryption key to whom the invite will be encrypted.  Must be a buffer or hex encoded string.
- `address` - the address of the space to be included in the invite.  Must be a buffer or hex encoded string.
- Optional: `encryptionKey` - the encryption key to whom the invite will be encrypted.  Must be a buffer or hex encoded string.
- Optional: `name` - a name for the space.  Must be a string 

### `{ address, encryptionKey, name } = open(opts)`

`opts = { invite, keypair }`

Decrypts and unpacks an invite code.
- `inviteCode` should be a buffer.
- keypair should be an object of the form `{ publicKey, secretKey }` both of which must be buffers or hex encoded strings.

### `send(recipientKey, inviteCode, opts = {}, callback)`
- `recipientKey` - the public encryption key of the intended recipient of the invite. Must be a buffer or hex encoded string.
- `inviteCode` an invite code which should be a buffer
- `callback` a function which will be called when the invite has been transmitted. This may take some time depending on whether they are currently online.
- `opts` is an optional object which may include
  - `swarm` - a custom swarm function, which behaves similarly to `cobox-swarm`

### `receive(publicKey, opts = {}, callback)` 
- `publicKey` - the public encryption key of the recipient of the invite. Must be a buffer or hex encoded string.
- `callback(err, inviteCode)` a function which will be called when the invite has been received.
  - `inviteCode` an invite code which will be a buffer
- `opts` is an optional object which may include
  - `swarm` - a custom swarm function, which behaves similarly to `cobox-swarm`

## API
See swagger documentation... (we won't have this for a while).

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
