const hypercore = require('hypercore')
const { keyPair } = require('hypercore-crypto')
const crypto = require('@coboxcoop/crypto')
const ram = require('random-access-memory')
const CoboxSwarm = require('./swarm')

const DISTINCTIVE = Buffer.from('peer2peerforeverpeer2peerforever') // 32 bytes

module.exports = { create, open, send, receive }

function create (opts = {}) {
  const {
    publicKey,
    address,
    encryptionKey,
    name = ''
  } = opts

  const packed = encryptionKey
    ? Buffer.concat([crypto.pack(address, encryptionKey), Buffer.from(name)])
    : Buffer.concat([address, Buffer.from(name)])

  return crypto.box(publicKey, packed)
}

function open (code, keyPair) {
  const kp = {}
  kp.publicKey = keyPair.publicKey
  kp.secretKey = keyPair.secretKey
  const packedWithName = crypto.unbox(code, kp)
  const packed = packedWithName.slice(0, crypto.PACKEDLENGTH)
  const name = packedWithName.slice(crypto.PACKEDLENGTH, packedWithName.length).toString()
  const unpacked = crypto.unpack(packed)
  if (name.length) unpacked.name = name

  return unpacked
}

function send (recipientKey, inviteCode, opts = {}, callback) {
  if (typeof opts === 'function' && !callback) {
    callback = opts
    opts = {}
  }
  const Swarm = opts.swarm || CoboxSwarm

  const { publicKey, secretKey } = keyPair(crypto.genericHash(recipientKey, DISTINCTIVE))
  const inviter = hypercore(ram, publicKey, { secretKey })

  inviter.on('ready', () => {
    inviter.append(inviteCode, (err) => {
      if (err) return callback(err)
      const swarm = Swarm(inviter) // { live: false }
      inviter.on('upload', () => {
        // should we wait a moment before destroying the stream?
        swarm.leave(inviter.discoveryKey)
        swarm.destroy(callback)
      })
    })
  })
}

function receive (ownPublicKey, opts = {}, callback) {
  if (typeof opts === 'function' && !callback) {
    callback = opts
    opts = {}
  }
  const Swarm = opts.swarm || CoboxSwarm

  const { publicKey } = keyPair(crypto.genericHash(ownPublicKey, DISTINCTIVE))
  const inviter = hypercore(ram, publicKey)
  inviter.on('ready', () => {
    const swarm = Swarm(inviter) // live: false, ack: true?

    inviter.update(() => { // or use .on('append')
      inviter.head((err, inviteCode) => {
        // check that the code looks like an invitecode?
        swarm.leave(inviter.discoveryKey)
        swarm.destroy(() => {
          callback(err, inviteCode) // toString('hex')
        })
      })
    })
  })
}
